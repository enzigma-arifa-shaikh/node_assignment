const assert = require('assert');
import  { newUser } from './../ts_Module/register'
import * as cha from 'chai';

const expect=cha.expect;

describe("register user",function(){
    let obj ={
        username :'abc',
        password:'lll',
        firstName:'hhh',
        lastName:'uuu',
        address:'kkk'
    };
    it('app should return a new user', async function(){
        let obj1= await newUser(obj);
        expect(obj1).to.be.a('object');
        expect(obj1).not.to.be.null;
        });
        
    });
