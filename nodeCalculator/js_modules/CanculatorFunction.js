"use strict";
exports.__esModule = true;
var calci = /** @class */ (function () {
    function calci() {
    }
    calci.addition = function (num1, num2) {
        return num1 + num2;
    };
    calci.substraction = function (num1, num2) {
        return num1 - num2;
    };
    calci.multiplication = function (num1, num2) {
        return num1 * num2;
    };
    calci.division = function (num1, num2) {
        return num1 / num2;
    };
    return calci;
}());
exports.calci = calci;
