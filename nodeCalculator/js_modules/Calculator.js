"use strict";
exports.__esModule = true;
var CanculatorFunction_1 = require("./CanculatorFunction");
var readlineSync = require('readline-sync');
console.log('*********CALCULATION MENU**********');
console.log('1. Addition\n2. Substraction\n3. Multiplication\n4. Division');
var choice = readlineSync.question('Enter Your choice :');
var number1 = readlineSync.question('Enter First Number :');
var number2 = readlineSync.question('Enter Second Number :');
var num1 = Number(number1);
var num2 = Number(number2);
switch (choice) {
    case "1":
        var addition = CanculatorFunction_1.calci.addition(num1, num2);
        console.log("Addition =", addition);
        break;
    case "2":
        var sub = CanculatorFunction_1.calci.substraction(num1, num2);
        console.log("Substraction =", sub);
        break;
    case "3":
        var mul = CanculatorFunction_1.calci.multiplication(num1, num2);
        console.log("Multiplication =", mul);
        break;
    case "4":
        var div = CanculatorFunction_1.calci.division(num1, num2);
        console.log("Division", div);
        break;
}
