//import * as mongoose from 'mongoose';
import traineeSchema from '../objects/traineeSchema';
//mongoose.connect("mongodb://localhost:27017/traineesDB",{useNewUrlParser:true});
const mongoose = require('mongoose');

export const databaseConnection = () =>{
  return mongoose.connect('mongodb://localhost:27017/traineesDB',{useNewUrlParser:true}, (err:any) => {
    if (err) {
      console.log(err.message);
      console.log(err);
    }
    else {
      console.log('Connected to MongoDb');
    }
  });
}

 let traineeModel = mongoose.model("traineesCollection" ,traineeSchema);
 export default traineeModel;



