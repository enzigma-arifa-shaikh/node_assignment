import mocha from 'mocha';
import expect from 'expect';
import { app } from '../startServer';
import router from '../routing/route';
import response from 'express';
import request from 'supertest';

describe("start Request", function (){
    it("Requesct from new trainee", async function ()
    {
        this.timeout(10000);
        const response: any = await request(app).get("/getTrainee").
            set('Accept', 'application.json');
        expect(200);
        expect(response.body).toBe("new employee");
    })
    it("request to create new trainee", function (done) {
        this.timeout(10000);
      const trainee = {
            traineeName: 'kuttu',
            traineeQualification: 'BE',
            traineeJoiningdate: 1 / 1 / 2019,
            traineeTrainings: '',
            traineeTrainingStatus: 'pass',
            traineeAllocatedproject: 'wework',
            traineeTrainingScore: 100
        }
        const response: any =
            request(app)
                .post("/newTrainee")
                .send(trainee).
                set('Accept', 'application/json').expect(200).end(done);
        expect(200);

    })
    it("Display all trainee", function (done) {
        this.timeout(90000);
        const response: any = request(app).
            get('/displayTrainee').
            set('Accept', 'application/json')
            .expect(200).
            end((err, res) => {
                console.log(res.body);
                done();
            });
        expect(response.body).not.toBeNull;
    })
    it("Delete Record from trainee", function (done) {
        this.timeout(90000);
        const sample = {
            "_id": new Object("5d32cd873d407d0ba8975230")

        }
        const response: any = request(app).
            post('/removeTrainee').
            set('Accept', 'application.json').send(sample).
            expect(200).
            end((err, res) => {
                console.log('In delete', res.body)
                expect(res.body);
                done();

            });

    })
    it("Update Record from trainee", function (done) {
        this.timeout(90000);
        const sample = {
            "_id": new Object("5d32c8077b65e533e4a3c26a")

        }
        const response: any = request(app).
            post('/updateTrainee').
            set('Accept', 'application.json').send(sample).
            expect(200).
            end((err, res) => {
                console.log(res.body);
                done();
            });
    })
})
