import express from 'express';
import * as path from 'path';
export const app = express();
import traineeModel from './model/traineeModel';
import router from './routing/route';
import * as bodyParser from 'body-parser';
import { databaseConnection } from './model/traineeModel'

let port = 4000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

databaseConnection().then(() => {
  app.listen(port, () => {
    console.log("Server starting...")
  })
})
app.use('/', router);
