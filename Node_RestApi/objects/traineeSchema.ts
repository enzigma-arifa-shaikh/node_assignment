import * as mongoose from 'mongoose'
const schema = mongoose.Schema;
  let taineeSchema = new schema({
    traineeName:{type:String,required:true},
    traineeQualification:{type:String,required:true},
    traineeJoiningdate:{type:Date,required:true},
    traineeTrainings:{type:Array,required:true,default:'salesforce admin and Apex'},
    traineeTrainingStatus:{type:String },
    traineeAllocatedproject: String,
    traineeTrainingScore:Number
});

export default taineeSchema;