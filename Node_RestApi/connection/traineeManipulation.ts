import traineeModel from '../model/traineeModel'
const Person = require("../model/traineeModel");
export class traineeManipulation {
  public createTrainee (trainee: any) {
    console.log('trainee is', trainee);
    let obj = new traineeModel(trainee);
    return obj.save()
      .then((doc: any) => {
        console.log(doc)
        return doc;
      })
      .catch((err: any) => {
        console.log(err);
        return err;
      })
  }

  public async  displayTrainee () {
    const result = await traineeModel.find({ "traineeName": "kuttu" });
    console.log(result)
    return result;
  }
  public async  removeTrainee (traineeId: any) {
    console.log('id of object', traineeId);
    const re = await traineeModel.deleteOne(traineeId);
    console.log(re);
    return re;
    //db.orders.deleteOne( { "_id" : ObjectId("563237a41a4d68582c2509da") } );
  }
  public async    editTrainee (newTrainee: any) {
    console.log('id of object', newTrainee);
    const res = traineeModel.findOneAndUpdate(
      { "traineeName": "kuttu" },
      {
        $set: { "traineeName": "shlok" },
        upsert: true, returnNewDocument: true
      }
    );
    return res;
    //console.log(res);

  }
}




