 import * as express from 'express';
import { Request, Response } from 'express';
import * as hbs from 'hbs';
import newTrainee from '../controller/traineeClass';
import taineeSchema from '../objects/traineeSchema';
const router: any = express.Router();
import traineeModel from '../model/traineeModel';
import { traineeManipulation } from '../connection/traineeManipulation';
const trainneObj = new traineeManipulation();

router.get ('/getTrainee', function (request: Request, response: Response){
    response.contentType('application/json');
    response.json('new employee')
});
router.post ('/newTrainee', function (request: Request, response: Response){
    const newtrainee = trainneObj.createTrainee(request.body);
    newtrainee.then (function (result: any) {
        response.status(200).send(result);
    }).catch(function (error: any) {
        response.sendStatus(404);
    })
    return response;
});
router.get('/displayTrainee', async function (request: Request, response: Response){
    const obj = await trainneObj.displayTrainee();
    //console.log('data of obj', obj);
    response.status(200).send(obj);
});
router.post('/removeTrainee', async function (request: Request, response: Response){
    const obj = await trainneObj.removeTrainee(request.body);
    //console.log('data of obj', obj);
    response.status(200).send(obj);
});
router.post('/updateTrainee', async function (request: Request, response: Response){
    const obj = await trainneObj.editTrainee(request.body);
    //console.log('data of obj', obj);
    response.status(200).send(obj);
})


export default router;
