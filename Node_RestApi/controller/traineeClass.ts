import { traineeManipulation } from '../connection/traineeManipulation';
let obj = new traineeManipulation();
class TraineeClass {
  public async newTrainee (newTrainee: any) {
    await obj.createTrainee(newTrainee);
  }
  public async  allTrainee () {
    await obj.displayTrainee();
  }
  public async  updateTrainee (newTrainee: any) {
    await obj.editTrainee(newTrainee);;
  }
  public async removeTrainee (traineeId: any) {
    await obj.removeTrainee (traineeId);
  }

}
export default TraineeClass;