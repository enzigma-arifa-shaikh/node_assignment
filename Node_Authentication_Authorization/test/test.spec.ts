import mocha from 'mocha';
import expect from 'expect';
import { app } from '../startServer';
import router from '../router/router';
import response from 'express';
import request from 'supertest';

describe("start Request", function (){
    it("Request from new User", async function ()
    {
        this.timeout(10000);
        const response: any = await request(app).get("/getUser").
            set('Accept', 'application.json');
        expect(200);
        expect(response.body).toBe("new user");
    })
    it("request to create new User", function (done) {
        this.timeout(10000);
      const user = {
      
            userName: "hiii",
            userPassword: "shhellolok",
            userEmail:"hdello@gmail.com",
            userMobilenumber:"12345566"   
        }
        const response: any =
            request(app)
                .post("/newUser")
                .send(user).
                set('Accept', 'application/json').expect(200).end(done);
        expect(200);

    })
    
    xit("Delete record from User", function (done) {
        this.timeout(90000);
        const sample = {
           
                userName:"abc",
                userPassword:"abc",
                userEmail:"agggg123@gmail.com",
                userMobilenumber:"123455"
        
        }
        const response: any = request(app).
            post('/removeUser').
            set('Accept', 'application.json').send(sample).
            expect(200).
            end((err, res) => {
                console.log('In delete', res.body)
                expect(res.body);
                done();

            });

    })
    xit("Update Record from User", function (done) {
        this.timeout(90000);
        const sample = {
            userName: "shlokt",
            userPassword: "shlhhokt",
            userEmail:"shlokjjjtg@gmail.com",
            userMobilenumber:"123455666"
        }
        const response: any = request(app).
            post('/editUser').
            set('Accept', 'application.json').send(sample).
            expect(200).
            end((err, res) => {
                console.log(res.body);
                done();
            });
    })
    xit("Validate User", function (done) {
        this.timeout(90000);
        const sample = {
            userName: "hiii",
            userPassword: "shhellolok",
            userEmail:"hello@gmail.com",
            userMobilenumber:"12345566" 
        }
        const response: any = request(app).
            post('/validateUser').
            set('Accept', 'application.json').send(sample).
            expect(200).
            end((err, res) => {
                console.log(res.body);
                done();
            })
    })
})
