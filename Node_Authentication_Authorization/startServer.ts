import express from 'express';
import * as path from 'path';
export const app = express();
import traineeModel from './model/userCollection';
import router from './router/router';
import * as bodyParser from 'body-parser';
import { databaseConnection } from './model/userCollection';

let port = 7000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

databaseConnection().then(() => {
  app.listen(port, () => {
    console.log("Server starting...")
  })
})
app.use('/', router);
