import userModel from '../model/userCollection';
import { Schema } from 'mongoose';
import  userSchema  from '../model/userCollection';


export class user{
    public  createUser(user: any):Promise<any> {
        console.log('User is', user);
        let obj = new userModel(user);
     
        const result= obj.save();
        console.log('obj is',result);
        return result;
      }
     public validateUser (user:any):Promise<any>{
       console.log('User is',user);
       let useremail = user.userEmail;
      // console.log('email is',useremail);
       return userModel.find({"userEmail":useremail}).then((doc: any) => {
        console.log(doc)
        return doc;
      })
      .catch((err: any) => {
        console.log(err);
        return err;
      })
       
     }

     public editMyUser(user:any): Promise<any>{
    return userModel.updateOne(user).then((doc: any) => {
      console.log(doc)
      return doc;
    })
    .catch((err: any) => {
      console.log(err);
      return err;
    })
     
  }
  public removeUser(user:any):Promise<any>{
    return userModel.deleteOne(user).then((doc: any) => {
      console.log(doc)
      return doc;
    })
    .catch((err: any) => {
      console.log(err);
      return err;
    })
     
  }
}
    
/*
 return obj.save()
      .then((doc: any) => {
        console.log(doc)
        return doc;
      })
      .catch((err: any) => {
        console.log(err);
        return err;
      })
  }*/