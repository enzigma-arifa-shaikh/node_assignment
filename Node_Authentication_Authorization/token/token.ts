var jwt = require('jsonwebtoken');
var keypair = require('keypair');
var fs = require('fs');

var pair = keypair();

var payload = {
    data1: 'abcd'
}
function saveSecretKey() {
    let privateKey = pair.private;
    let publicKey = pair.public;
    fs.writeFileSync('./private.key', privateKey);
    fs.writeFileSync('./public.key', publicKey);
}
var token: any;
export function generateToken() {
    saveSecretKey();
    var privateKEY = fs.readFileSync('./private.key', 'utf8');
    token = jwt.sign(payload, privateKEY, { algorithm: 'RS256' });
    console.log('token====>', token);
    return token;
}
export function verifyToken(token : any) {
    var publicKEY = fs.readFileSync('./public.key', 'utf8');
    let verify = jwt.verify(token, publicKEY);
   // console.log('verify is====>', verify);
}

//generateToken();
//verifyToken();

//console.log('private key =>',privateKey);
//console.log('public key =>',publicKey);   
//var privateKEY  = fs.readFileSync('./private.key', 'utf8');
//console.log('Reading file Private Key',privateKEY);
//var publicKEY  = fs.readFileSync('./public.key', 'utf8');
//console.log('Reading file public Key',publicKEY);