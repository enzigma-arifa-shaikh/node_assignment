//import * as mongoose  from "mongoose"

var mongoose = require('mongoose');
const schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');
let userSchema = new schema ( {
    userName:
    {
      type:String,
      required:true
    },
    userPassword:
    {
      type:String,
      required:true
    },
    userEmail:{
      type:String,
      required:true,
      trim:true,
      unique:true,
      uniqueCaseInsensitive: true
    },
    userMobilenumber:
    {
      type:Number,
      required:true
    },
    
} );
userSchema.plugin(uniqueValidator);

export default userSchema;



