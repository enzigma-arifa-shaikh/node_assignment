import * as express from 'express';
import { Request, Response } from 'express';
import * as hbs from 'hbs';
import { user } from '../Controller/user';
import { generateToken } from '../token/token';
import { verifyToken } from '../token/token';
import taineeSchema from '../schema/userSchema';
const router: any = express.Router();
import traineeModel from '../model/userCollection';

const trainneObj = new user();

router.get('/getUser', function (request: Request, response: Response) {
    response.contentType('application/json');
    response.json('new user')
});
router.post('/newUser', async function (request: Request, response: Response) {

    const newtrainee = await trainneObj.createUser(request.body);
    console.log('result ', newtrainee);
    if (newtrainee) {
        response.header("content-type", "application/json");
        let token = generateToken();
        console.log('token is =====>', token);
        response.setHeader('header', token);
        response.send(newtrainee);
    }
    else {
        response.sendStatus(404);
    }
});
router.post('/validateUser', async function (request: Request, response: Response) {
    const verifytoken = request.headers["token"];
    const check = await verifyToken(verifytoken);
    const newtrainee = await trainneObj.validateUser(request.body);
    console.log('result ', newtrainee);
    if (newtrainee) {
        response.send(newtrainee);
        console.log('valid user');
    }
    else {
        response.sendStatus(404);
        console.log('User is Invalid')
    }


});
router.post('/editUser',async function(request:Request,response:Response){
    const verifytoken = request.headers["token"];
    const check = await verifyToken(verifytoken);  
    const newtrainee =await trainneObj.editMyUser(request.body);
    console.log(newtrainee);
    if(newtrainee){
        response.send(newtrainee);
        response.sendStatus(200);
        console.log('User Updated'); 
    }
    else{
        response.sendStatus(404);
    }
});
router.post('/removeUser',async function(request:Request,response:Response){
    //const verifytoken = request.headers["token"];
   // const check = await verifyToken(verifytoken);  
    const newtrainee = await trainneObj.removeUser(request.body);
    console.log(newtrainee);
    if(newtrainee){
        response.send(newtrainee);
        console.log('User Deleted'); 
    }
    else{
        response.sendStatus(404); 
    }
    
    
 });



export default router;
