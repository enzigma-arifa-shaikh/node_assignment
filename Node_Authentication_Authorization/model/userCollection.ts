import userSchema from "../schema/userSchema";

const mongoose = require ("mongoose");

export const databaseConnection = () => {
  return mongoose.connect("mongodb://localhost:27017/MyUserDatabase", {useNewUrlParser: true}, (err: any) => {
    if (err) {
      console.log(err.message);
      console.log(err);
    }
    else {
      console.log('Connected to MongoDb');
    }
  });
}

let userModel  = mongoose.model("usersCollection" ,userSchema);
export default userModel ;