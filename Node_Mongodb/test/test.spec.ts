const assert = require('assert');
import {describe} from 'mocha';

import * as cha from 'chai';
const expect=cha.expect;
import { userManagement } from '../dbcontroller/connection';
import {userId} from '../object/userId';
describe("User operation" ,function(){
   it("creating new user",async function(){
        let user = new userManagement();
        
        let response = await user.createUser("UserDatabase",userId);
       expect(response).not.to.equal(response.error,"Not expected Result");
        
    })
    it("Displaying user",async function(){
        let user = new userManagement();
        
        let response = await user.DisplayUser("UserDatabase");
       expect(response).not.to.null;
        
    })
    it("Deleting user",async function(){
        let user = new userManagement();
        var myquery = {  empId:'344' };
       
        let response: any = await user.removeUser("UserDatabase",myquery);
        
            console.log('in test '+response);
           
            expect(response.deletedCount).greaterThan(0);
           
        
         
        
    })
    it("Updating user",async function(){
        let user = new userManagement();
        var myquery = {  empId:'344' };
       
        let response: any = await user.editUser("UserDatabase",myquery);
        console.log("response in updating"+response);
        expect(response.result.nModified).greaterThan(0);
        //expect(response).not.to.null;
          
         
        
    })
    it("Return user",async function(){
        let user = new userManagement();
        var myquery = {  empId:'344' };
       
        let response: any = await user.editUser("UserDatabase",myquery);
        console.log("response in return user"+response);
        expect(response.result.ok).greaterThan(0);
        
          
         
        
    })

    })