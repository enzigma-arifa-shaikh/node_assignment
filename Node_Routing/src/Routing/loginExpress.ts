import * as express from 'express';
import {Request,Response} from 'express';
import * as hbs from 'hbs'; 
//const application : any = express();
let router:any=express.Router();

router.get('/home',function(request: Request,response: Response){
    response.render('login.hbs');
  
});
router.get('/signUp',function(request: Request,response: Response){
    response.render('signUp.hbs');
  
});
router.get('/changePassword',function(request: Request,response: Response){
    response.render('changePassword.hbs');
  
});
router.get('/forgetPassword',function(request: Request,response: Response){
    response.render('forgetPassword.hbs');
  
});

export default router;