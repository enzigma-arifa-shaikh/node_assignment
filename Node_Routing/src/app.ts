import express from "express";
import * as path from 'path';
const app = express();
import router from './Routing/loginExpress';
let port = 4400;

app.set('view engine','.hbs');
app.use(express.static(path.join(__dirname,'./views')));
app.use(express.static(path.join(__dirname,'./public')));
app.use('/',router);

app.listen(port,() =>{
    console.log("Server starting...")
})

